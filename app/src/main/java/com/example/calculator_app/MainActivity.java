package com.example.calculator_app;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView addScreen;
    private Button bt, btNumber0, btNumber1, btnNumber2, btNumber3, btNumber4, btNumber5
            ,btNumber6, btNumber7, btNumber8, btNumber9, btAC, btPlus, btMod
                , btAdd, btSub, btMul, btDiv, btResult;
    private String answer;
    private Boolean activeMod, checkOp, checkNum, checkPlusMinus, checkDouble, checkBt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkOp = checkNum = activeMod = false;
        checkPlusMinus = checkDouble = checkBt = false;
        //TextView
        addScreen = findViewById(R.id.screen);
        addScreen.setOnClickListener(this);
        //Button Numbers
        bt = findViewById(R.id.btn);
        btNumber0 = findViewById(R.id.btn_number_0);
        btNumber1 = findViewById(R.id.btn_number_1);
        btnNumber2 = findViewById(R.id.btn_number_2);
        btNumber3 = findViewById(R.id.btn_number_3);
        btNumber4 = findViewById(R.id.btn_number_4);
        btNumber5 = findViewById(R.id.btn_number_5);
        btNumber6 = findViewById(R.id.btn_number_6);
        btNumber7 = findViewById(R.id.btn_number_7);
        btNumber8 = findViewById(R.id.btn_number_8);
        btNumber9 = findViewById(R.id.btn_number_9);
        bt.setOnClickListener(this);
        btNumber0.setOnClickListener(this);
        btNumber1.setOnClickListener(this);
        btnNumber2.setOnClickListener(this);
        btNumber3.setOnClickListener(this);
        btNumber4.setOnClickListener(this);
        btNumber5.setOnClickListener(this);
        btNumber6.setOnClickListener(this);
        btNumber7.setOnClickListener(this);
        btNumber8.setOnClickListener(this);
        btNumber9.setOnClickListener(this);
        //Button Operation
        btMod = findViewById(R.id.btn_mod);
        btAdd = findViewById(R.id.btn_add);
        btSub = findViewById(R.id.btn_sub);
        btMul = findViewById(R.id.btn_mul);
        btDiv = findViewById(R.id.btn_div);
        btMod.setOnClickListener(this);
        btAdd.setOnClickListener(this);
        btSub.setOnClickListener(this);
        btMul.setOnClickListener(this);
        btDiv.setOnClickListener(this);
        //Button Result
        btResult = findViewById(R.id.btn_result);
        btResult.setOnClickListener(this);
        //Button Clear
        btAC = findViewById(R.id.btn_clean_ac);
        btPlus = findViewById(R.id.btn_plus_minus);
        btAC.setOnClickListener(this);
        btPlus.setOnClickListener(this);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onClick(View v) {
        String data = addScreen.getText().toString();
        answer = data;
        switch (v.getId()) {
            case R.id.btn_clean_ac:
                checkOp = checkNum = checkDouble = checkBt = activeMod = false;
                if(data.length() > 0)
                    answer = "";
                break;
            case R.id.btn:
                activeMod = checkNum = true;
                if(!data.isEmpty() && !checkBt) {
                    answer +=",";
                }
                checkBt = true;
                break;
            case R.id.btn_plus_minus:
                if(!data.isEmpty() && !checkPlusMinus) {
                    answer = "-" + answer;
                    checkPlusMinus = true;
                }else if(checkPlusMinus) {
                    answer = "" + answer.replaceFirst("-", "");
                    checkPlusMinus = false;
                }
                break;

            case R.id.btn_mod:
            case R.id.btn_result:
                //                if(!data.isEmpty()) {
//                    Double percentRes = Double.parseDouble(data)/100;
//                    answer = String.valueOf(percentRes);
//                    checkDouble = true;
//                }
                Solve();
                activeMod = checkBt = checkNum = false;
                break;

            case R.id.btn_add:
                checkBt = false;
                //String dataAdd = btAdd.getText().toString();
                if(data.length() > 0 && !data.equals("0") && !checkOp) {
                    answer += " " + "+" + " ";
                    Solve();
                    checkOp = true;
                }
                if(!data.equals("*") && !data.equals("-") && !data.equals("/") && !data.isEmpty()) {
                    Solve();
                    if(!checkOp) {
                        answer += " " + "+" + " ";
                    }
                    checkOp = true;
                }
                break;

            case R.id.btn_sub:
                checkBt = false;
                if(data.length() > 0 && !data.equals("0")  && !checkOp) {
                    answer += " " + "-" + " ";
                    Solve();
                    checkOp = true;
                }
                if(!data.equals("+") && !data.equals("*") && !data.equals("/") && !data.isEmpty()) {
                    Solve();
                    if(!checkOp) {
                        answer += " " + "-" + " ";
                    }
                    checkOp = true;
                }
                break;

            case R.id.btn_mul:
                checkBt = false;
                if(data.length() > 0 && !data.equals("0") && !checkOp) {
                    answer += " " + "*" + " ";
                    Solve();
                    checkOp = true;
                }
                if(!data.equals("+") && !data.equals("-") && !data.equals("/") && !data.isEmpty()) {
                    Solve();
                    if(!checkOp) {
                        answer += " " + "*" + " ";
                    }
                    checkOp = true;
                }
                break;

            case R.id.btn_div:
                checkBt = false;
                if(data.length() > 0 && !data.equals("0") && !checkOp) {
                    answer += " " + "/" + " ";
                    Solve();
                    checkOp = true;
                }
                if(!data.equals("+") && !data.equals("-") && !data.equals("*") && !data.isEmpty()) {
                    Solve();
                    if(!checkOp) {
                        answer += " " + "/" + " ";
                    }
                    checkOp = true;
                }
                //checkDouble = true;
                checkBt = false;
                break;

            case R.id.btn_number_0:
                if(checkNum)
                    answer = answer + getString(R.string.number_0);
                break;

            case R.id.btn_number_1:
                answer = answer.replaceFirst("01", "1") + getString(R.string.number_1);
                checkNum = true;
                break;

            case R.id.btn_number_2:
                answer = answer.replaceFirst("02", "2") + getString(R.string.number_2);
                checkNum = true;
                break;

            case R.id.btn_number_3:
                answer = answer.replaceFirst("03", "3") + getString(R.string.number_3);
                checkNum = true;
                break;

            case R.id.btn_number_4:
                answer = answer.replaceFirst("04", "4") + getString(R.string.number_4);
                checkNum = true;
                break;

            case R.id.btn_number_5:
                answer = answer.replaceFirst("05", "5") + getString(R.string.number_5);
                checkNum = true;
                break;

            case R.id.btn_number_6:
                answer = answer.replaceFirst("06", "6") + getString(R.string.number_6);
                checkNum = true;
                break;

            case R.id.btn_number_7:
                answer = answer.replaceFirst("07", "7") + getString(R.string.number_7);
                checkNum = true;
                break;

            case R.id.btn_number_8:
                answer = answer.replaceFirst("08", "8") + getString(R.string.number_8);
                checkNum = true;
                break;

            case R.id.btn_number_9:
                answer = answer.replaceFirst("09", "9") + getString(R.string.number_9);
                checkNum = true;
                break;

        }
            addScreen.setText(answer);
        }

    public void Solve() {
//        if(btMod.isClickable() && !checkDouble) {
//            String number[] = answer.split(" // ");
//            Double percentRes = Double.parseDouble(number[0])/100 + Double.parseDouble(number[1])/100;
//            answer = String.valueOf(percentRes);
//            //checkDouble = true;
//        }
        if (btAdd.isClickable() && answer.split(" \\+ ").length == 2) {
            String number[] = answer.split(" \\+ ");
            if(!activeMod) {
                Integer add = Integer.parseInt(number[0]) + Integer.parseInt(number[1]);
                answer = add + "";
            } else {
                Double addD = Double.parseDouble(number[0].replaceAll(",", ".")) + Double.parseDouble(number[1].replaceAll(",", "."));
                String a = String.valueOf(addD);
                answer = a.replace('.', ',') + "";
            }
            checkOp = false;
        }

        if (btSub.isClickable() && answer.split(" \\- ").length == 2) {
            String number[] = answer.split(" \\- ");
            try {
                if(!activeMod) {
                    Integer sub = Integer.parseInt(number[0]) - Integer.parseInt(number[1]);
                    answer = sub + "";
                } else {
                    Double subD = Double.parseDouble(number[0].replaceAll(",", ".")) - Double.parseDouble(number[1].replaceAll(",", "."));
                    String a = String.valueOf(subD);
                    answer = a.replace('.', ',') + "";
                }
            } catch (Exception e) {
            }
            checkOp = false;
        }

        if (btMul.isClickable() && answer.split(" \\* ").length == 2) {
            String number[] = answer.split(" \\* ");
            if(!activeMod) {
                Integer mul = Integer.parseInt(number[0]) * Integer.parseInt(number[1]);
                answer = mul + "";
            }
            else {
                Double mulD = Double.parseDouble(number[0].replaceAll(",", ".")) * Double.parseDouble(number[1].replaceAll(",", "."));
                String a = String.valueOf(mulD);
                answer = a.replace('.', ',') + "";
            }
            checkOp = false;
        }

        if (btDiv.isClickable() && answer.split(" \\/ ").length == 2) {
            String number[] = answer.split(" \\/ ");
                try {
                    if(!activeMod) {
                        Integer div = Integer.parseInt(number[0]) / Integer.parseInt(number[1]);
                        answer = div + "";
                    }
                    else {
                        Double divD = Double.parseDouble(number[0].replaceAll(",", ".")) / Double.parseDouble(number[1].replaceAll(",", "."));
                        String a = String.valueOf(divD);
                        answer = a.replace('.', ',') + "";
                    }
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "Not a number", Toast.LENGTH_SHORT).show();
                    answer = "";
                }
//            if(!bt.isClickable()) {
//                try {
//                    Integer div = Integer.parseInt(number[0]) / Integer.parseInt(number[1]);
//                    answer = div + "";
//                } catch (Exception e) {
//                    Toast.makeText(MainActivity.this, "Not a number", Toast.LENGTH_SHORT).show();
//                    answer = "";
//                }
//                //checkDouble = true;s
//            }
//             else if(bt.isActivated())
//             {
//                Double divD = Double.parseDouble(number[0].replaceAll(",", ".")) / Double.parseDouble(number[1].replaceAll(",", "."));
//                String a = String.valueOf(divD);
//                answer = a.replace('.', ',') + "";
//            }
            checkOp = false;
        }
    }
}